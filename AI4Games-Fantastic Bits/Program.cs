﻿using System;
using System.Collections.Generic;
using System.Linq;

/**
 * Grab Snaffles and try to throw them through the opponent's goal!
 * Move towards a Snaffle and use your team id to determine where you need to throw it.
 **/
namespace AI4Games_Fantastic_Bits
{
    public class Coordinates
    {
        public static Coordinates operator +(Coordinates c1, Coordinates c2) => new Coordinates(c1.X + c2.X, c1.Y + c2.Y);

        public static Coordinates operator -(Coordinates c1, Coordinates c2) => new Coordinates(c1.X - c2.X, c1.Y - c2.Y);

        public Coordinates(int x, int y)
        {
            /*
            if (x < 0)
                x = 0;
            if (x > MaxX)
                x = MaxX;
            if (y < 0)
                y = 0;
            if (y > MaxY)
                y = MaxY;
            */
            X = x;
            Y = y;
        }

        public static int MaxX { get; } = 16000;

        public static int MaxY { get; } = 7500;

        public int X { get; set; }

        public int Y { get; set; }

        public double Distance(Coordinates c)
        {
            return Math.Sqrt(Math.Pow(X - c.X, 2) + Math.Pow(Y - c.Y, 2));
        }

        public Coordinates Opposite(Coordinates coordinates)
        {
            return new Coordinates(coordinates.X - 2 * X, coordinates.Y - 2 * Y);
        }
    }

    public interface IEntity
    {
        void Update(Coordinates coordinates, Coordinates velocity, int state);
        int Id { get; set; }
    }

    public class Wizard : IEntity
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public Coordinates Coordinates { get; set; }

        public Coordinates Velocity { get; set; }

        public int State { get; set; }

        public GameState GameState_ { private get; set; }

        public const int MaxThrust = 150;

        public const int MaxPower = 500;

        public const int AccioCost = 20;

        public const int FlipendoCost = 20;

        public const int PetrificusCost = 10;

        public Ball CastedFlipendo { get; set; }

        public int TimeFromFlipendo { get; set; }

        public Wizard(int id, string type)
        {
            Id = id;
            Type = type;
            TimeFromFlipendo = 20;
        }

        public void Update(Coordinates coordinates, Coordinates velocity, int state)
        {
            Coordinates = coordinates;
            Velocity = velocity;
            State = state;
        }

        public void Move(Coordinates target, int thrust = MaxThrust)
        {
            Console.WriteLine("MOVE {0} {1} {2}", target.X, target.Y, thrust);
        }

        public void Throw(Coordinates target, int power = MaxPower)
        {
            Console.WriteLine("THROW {0} {1} {2}", target.X, target.Y, power);
        }

        public void CastAccio(Ball ball)
        {
            Console.WriteLine("ACCIO {0}", ball.Id);
            GameState.Mana -= AccioCost;
        }

        public void CastFlipendo(IEntity entity)
        {
            Console.WriteLine("FLIPENDO {0}", entity.Id);
            GameState.Mana -= FlipendoCost;
            CastedFlipendo = (Ball)entity;
            TimeFromFlipendo = 0;
        }

        public void CastPetrificus(IEntity entity)
        {
            Console.WriteLine("PETRIFICUS {0}", entity.Id);
            GameState.Mana -= PetrificusCost;
        }

        public void MakeMove()
        {
            Ball target = GameState_.NearestBall(this);
            bool targetOnGoodSide = GameState_.MyTeamId == 1
                ? target.Coordinates.X > Coordinates.X
                : target.Coordinates.X < Coordinates.X;
            double ballDistance = Coordinates.Distance(target.Coordinates);
            Wizard voldemort = GameState_.NearestEnemy(this);
            double enemyDistance = Coordinates.Distance(voldemort.Coordinates);

            Ball ballNearMyGoal = null;
            double minInMyGoalAfter = 20;
            int direction = GameState_.MyTeamId == 0 ? -1 : 1;
            Coordinates vectorToGoal = target.Coordinates - Coordinates;
            double angle = (double)vectorToGoal.Y / vectorToGoal.X;
            double destinationY = target.Coordinates.Y +
                                  (GameState_.EnemyGoalCoordinates.X - target.Coordinates.X) * angle;
            Console.Error.WriteLine("destinationY: {0}", destinationY);
            foreach (Ball snaffle in GameState_.Snaffles.Values)
            {
                double distanceToGoal = Math.Abs(snaffle.Coordinates.X - GameState_.MyGoalCoordinates.X);
                double inMyGoalAfter = direction * distanceToGoal / snaffle.Velocity.X;
                Coordinates nextCoordinates = snaffle.Coordinates + snaffle.Velocity;
                if (inMyGoalAfter > 0 && inMyGoalAfter < 3 && direction * snaffle.Velocity.X > 200 &&
                    (ballNearMyGoal == null || inMyGoalAfter < minInMyGoalAfter) &&
                    nextCoordinates.X > 50 && nextCoordinates.X < Coordinates.MaxX - 50)
                {
                    minInMyGoalAfter = inMyGoalAfter;
                    ballNearMyGoal = snaffle;
                }
            }
            bool correctAim = false;
            for (int i = -1; i <= 1; i++)
                correctAim = correctAim ||
                             (destinationY > GameState_.EnemyGoalCoordinates.Y - 1800 + i * Coordinates.MaxY &&
                              destinationY < GameState_.EnemyGoalCoordinates.Y + 1800 + i * Coordinates.MaxY);


            if (State == 1)
            {
                Throw(GameState_.EnemyGoalCoordinates);
            }
            else if (TimeFromFlipendo < 5 && CastedFlipendo != null)
            {
                Move(CastedFlipendo.Coordinates + CastedFlipendo.Velocity);
                CastedFlipendo.ChasedBy = this;
            }
            else if (GameState.Mana >= FlipendoCost && !targetOnGoodSide && ballDistance > 100 && ballDistance < 8000 &&
                target.Coordinates.X > 500 && target.Coordinates.X < Coordinates.MaxX - 500 &&
                correctAim)
            {
                CastFlipendo(target);
            }
            else if (GameState.Mana >= AccioCost && targetOnGoodSide && ballDistance > 1000)
            {
                CastAccio(target);
            }
            else if (GameState.Mana >= PetrificusCost && ballNearMyGoal != null && !GameState.PetrificusCastedOnBall)
            {
                CastPetrificus(ballNearMyGoal);
                GameState.PetrificusCastedOnBall = true;
            }
            /*else if (GameState.Mana >= FlipendoCost && ballDistance < 1000 && enemyDistance < 1000)
            {
                CastPetrificus(voldemort);
            }*/
            else
            {
                Move(target.Coordinates + target.Velocity);
                target.ChasedBy = this;
            }

            TimeFromFlipendo++;
        }
    }

    public class Ball : IEntity
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public Coordinates Coordinates { get; set; }

        public Coordinates Velocity { get; set; }

        public bool InGame { get; set; }

        public Wizard ChasedBy { get; set; }

        public bool PetrificusCastedOnBall { get; set; }
        
        public Ball(int id, string type)
        {
            Id = id;
            Type = type;
            PetrificusCastedOnBall = false;

        }

        public void Update(Coordinates coordinates, Coordinates velocity, int state)
        {
            Coordinates = coordinates;
            Velocity = velocity;
            InGame = true;
            ChasedBy = null;
        }
    }

    public class EntityFactory
    {
        public EntityFactory(GameState gameState)
        {
            _gameState = gameState;
        }

        private readonly GameState _gameState;

        public IEntity GetEntity(string entityType, int entityId)
        {

            if (entityType == "WIZARD")
            {
                if (!_gameState.Wizards.ContainsKey(entityId))
                {
                    _gameState.Wizards[entityId] = new Wizard(entityId, "WIZARD");
                    _gameState.Wizards[entityId].GameState_ = _gameState;
                }
                return _gameState.Wizards[entityId];
            }
            if (entityType == "OPPONENT_WIZARD")
            {
                if (!_gameState.EnemyWizards.ContainsKey(entityId))
                    _gameState.EnemyWizards[entityId] = new Wizard(entityId, "OPPONENT_WIZARD");
                return _gameState.EnemyWizards[entityId];
            }
            if (entityType == "SNAFFLE")
            {
                if (!_gameState.Snaffles.ContainsKey(entityId))
                    _gameState.Snaffles[entityId] = new Ball(entityId, "SNAFFLE");
                return _gameState.Snaffles[entityId];
            }
            if (entityType == "BLUDGER")
            {
                if (!_gameState.Bludgers.ContainsKey(entityId))
                    _gameState.Bludgers[entityId] = new Ball(entityId, "BLUDGER");
                return _gameState.Bludgers[entityId];
            }
            throw new NullReferenceException("Invalid entity type");
        }
    }

    public class GameState
    {
        public int MyTeamId { get; }

        public SortedDictionary<int, Ball> Snaffles;

        public SortedDictionary<int, Ball> Bludgers;

        public SortedDictionary<int, Wizard> Wizards;

        public SortedDictionary<int, Wizard> EnemyWizards;

        public Coordinates EnemyGoalCoordinates { get; set; }

        public Coordinates MyGoalCoordinates { get; set; }

        public static int Mana { get; set; }
        public static bool PetrificusCastedOnBall { get; set; }

        public GameState(int myTeamId)
        {
            MyTeamId = myTeamId;
            Snaffles = new SortedDictionary<int, Ball>();
            Bludgers = new SortedDictionary<int, Ball>();
            Wizards = new SortedDictionary<int, Wizard>();
            EnemyWizards = new SortedDictionary<int, Wizard>();
            EnemyGoalCoordinates = MyTeamId == 1
                ? new Coordinates(0, Coordinates.MaxY / 2)
                : new Coordinates(Coordinates.MaxX, Coordinates.MaxY / 2);
            MyGoalCoordinates = MyTeamId == 0
                ? new Coordinates(0, Coordinates.MaxY / 2)
                : new Coordinates(Coordinates.MaxX, Coordinates.MaxY / 2);
            Mana = 0;
            PetrificusCastedOnBall = false;
        }

        public Ball NearestBall(Wizard wizard)
        {
            Ball target = Snaffles.Values.ToArray()[0];
            double minDistance = wizard.Coordinates.Distance(target.Coordinates);
            foreach (Ball snaffle in Snaffles.Values)
            {
                int penalty = 5000;
                int distanceDisturbance = (snaffle.ChasedBy == null || snaffle.ChasedBy == wizard) ? 0 : penalty;
                if (wizard.Coordinates.Distance(snaffle.Coordinates + snaffle.Velocity) + distanceDisturbance < minDistance)
                {
                    minDistance = wizard.Coordinates.Distance(snaffle.Coordinates);
                    target = snaffle;
                }
            }
            return target;
        }
        

        public Wizard NearestEnemy(Wizard wizard)
        {
            Wizard target = EnemyWizards.Values.ToArray()[0];
            double minDistance = wizard.Coordinates.Distance(target.Coordinates);
            foreach (Wizard enemy in EnemyWizards.Values)
            {
                if (wizard.Coordinates.Distance(enemy.Coordinates) < minDistance)
                {
                    minDistance = wizard.Coordinates.Distance(enemy.Coordinates);
                    target = enemy;
                }
            }
            return target;
        }
    }

    class Player
    {
        static void Main(string[] args)
        {
            Console.Error.WriteLine(new Coordinates(5792, 1848).Distance(new Coordinates(10554, 3679)));
            int myTeamId = int.Parse(Console.ReadLine()); // if 0 you need to score on the right of the map, if 1 you need to score on the left
            Console.Error.WriteLine("{0}", myTeamId);
            GameState gameState = new GameState(myTeamId);
            EntityFactory entityFactory = new EntityFactory(gameState);

            // game loop
            while (true)
            {
                int entities = int.Parse(Console.ReadLine()); // number of entities still in game
                Console.Error.WriteLine("{0}", entities);
                gameState.Snaffles = new SortedDictionary<int, Ball>();
                for (int i = 0; i < entities; i++)
                {
                    string[] inputs = Console.ReadLine().Split(' ');
                    int entityId = int.Parse(inputs[0]); // entity identifier
                    string entityType = inputs[1]; // "WIZARD", "OPPONENT_WIZARD" or "SNAFFLE" (or "BLUDGER" after first league)
                    int x = int.Parse(inputs[2]); // position
                    int y = int.Parse(inputs[3]); // position
                    Coordinates coordinates = new Coordinates(x, y);
                    int vx = int.Parse(inputs[4]); // velocity
                    int vy = int.Parse(inputs[5]); // velocity
                    Coordinates velocity = new Coordinates(vx, vy);
                    int state = int.Parse(inputs[6]); // 1 if the wizard is holding a Snaffle, 0 otherwise

                    Console.Error.WriteLine("{0} {1} {2} {3} {4} {5} {6}", entityId, entityType, x, y, vx, vy, state);

                    IEntity entity = entityFactory.GetEntity(entityType, entityId);
                    entity.Update(coordinates, velocity, state);
                }

                foreach (Wizard wizzard in gameState.Wizards.Values)
                {

                    // Write an action using Console.WriteLine()
                    // To debug: Console.Error.WriteLine("Debug messages...");


                    // Edit this line to indicate the action for each wizard (0 <= thrust <= 150, 0 <= power <= 500)
                    // i.e.: "MOVE x y thrust" or "THROW x y power"
                    wizzard.MakeMove();
                    Console.Error.WriteLine();
                }
                GameState.Mana++;
                if (GameState.Mana % 3 == 0)
                    GameState.PetrificusCastedOnBall = false;
                foreach (Ball snaffle in gameState.Snaffles.Values)
                {
                    snaffle.InGame = false;
                }
            }
        }
    }
}